<?php

namespace Fit\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FitUserBundle extends Bundle
{
  public function getParent()
  {
      return 'FOSUserBundle';
  }
}
