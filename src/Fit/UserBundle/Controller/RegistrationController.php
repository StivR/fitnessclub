<?php
namespace Fit\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends BaseController
{
  public function registerAction(Request $request)
  {
    return $this->render('@FOSUser/Registration/register.html.twig', array(
        'form' => $form->createView(),
    ));
  }
}
