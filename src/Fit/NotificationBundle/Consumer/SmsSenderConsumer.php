<?php
namespace Fit\NotificationBundle\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class NotificationConsumer
 */
class SmsSenderConsumer implements ConsumerInterface
{
  private $delayedProducer;

  /**
   * MailSenderConsumer constructor.
   * @param ProducerInterface      $delayedProducer
   * @param EntityManagerInterface $entityManager
   */
  public function __construct(ProducerInterface $delayedProducer)
  {
      $this->delayedProducer = $delayedProducer;
  }
    /**
     * @var AMQPMessage $msg
     * @return void
     */
    public function execute(AMQPMessage $msg)
    {
        return $this->processMessage($msg);
    }

    /**
     * @param AMQPMessage $msg
     * @return int
     */
    public function processMessage(AMQPMessage $msg)
    {
        print_r($msg->getBody());
        $message = $msg->getBody();
        $sender = $this->sendSms($message);
        if(isset($sender['ERROR_'])){
          print_r($sender);
          $this->delayedProducer->publish($message);
        }else{
          print_r('send');
          print_r($sender);
        }
        return ConsumerInterface::MSG_ACK;
    }

    public function sendSms($msg)
    {
      $data = file_get_contents($msg);
      parse_str($data, $m);
      foreach ($m as $k => $v)
          $m[$k] = isset($v[0]) && $v[0] == "@" ? sprintf("\0%s", $v) : $v;
      return $m;
    }
}
