<?php

namespace Fit\NotificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SendController extends Controller
{
  /*
  * Email send Action
  */
  public function sendAction(Request $req)
  {
    $class = $req->get('class');
    $emsg = $req->get('emsg');
    $smsg = $req->get('smsg');
    $data = null;

    if($emsg){
      $emails = $this->getDoctrine()->getRepository('FitAdminBundle:UserClasses')->findBy(['classes' => $class, 'notif' => 1]);
    }else {
      $emails = null;
    }

    if($smsg){
      $sms = $this->getDoctrine()->getRepository('FitAdminBundle:UserClasses')->findBy(['classes' => $class, 'notif' => 2]);
    }else{
      $sms = null;
    }

    if($emails){
      foreach ($emails as $item) {
        if($item->getUser()->isEnabled()){
          $femsg = str_replace('%fullname%', $item->getUser()->getFullname(), $emsg);
          $femsg = str_replace('%phone%', $item->getUser()->getPhone(), $femsg);
          $message = (new \Swift_Message('FitnesClub Notification'))
              ->setFrom('support@fitdemo.ru')
              ->setTo($item->getUser()->getEmail())
              ->setBody($emsg)
          ;
          $this->get('mailer')->send($message);
        }
      }
      $data = ['status' => 'success'];
    }

    if($sms){
      foreach ($sms as $item) {
        if($item->getUser()->isEnabled()){
          $fsmsg = str_replace('%fullname%', $item->getUser()->getFullname(), $smsg);
          $fsmsg = str_replace('%phone%', $item->getUser()->getPhone(), $fsmsg);
          $this->container->get('old_sound_rabbit_mq.send_sms_producer')->publish('https://smsc.ru/sys/send.php?login='.urlencode('StivR').'&psw='.urlencode('5t4r3e2w1q').'&phones='.urlencode($item->getUser()->getPhone()).'&mes='.urlencode($fsmsg).'&charset=utf-8');
        }
      }
      $data = ['status' => 'success'];
    }

    if(!$data){ $data = ['status' => 'error']; }

    $serializer = $this->container->get('jms_serializer');
    $res = $serializer->serialize($data, 'json');

    return new Response($res);
  }
}
