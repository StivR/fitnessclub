<?php
namespace Fit\AdminBundle\Service;

use FOS\UserBundle\Util\TokenGeneratorInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserManagerInterface;

/**
 * Registration Service
 */
class RegService
{
  private $tokenGenerator;
  private $userManager;
  private $mailer;
  private $em;

  /**
  * @param TokenGeneratorInterface  $tokenGenerator
  * @param UserManagerInterface     $userManager
  * @param MailerInterface          $mailer
  */
  function __construct(TokenGeneratorInterface $tokenGenerator, UserManagerInterface $userManager, MailerInterface $mailer)
  {
    $this->tokenGenerator = $tokenGenerator;
    $this->mailer = $mailer;
    $this->userManager = $userManager;
  }

  /**
   * Request reset user password: submit form and send email.
   *
   * @param Request $request
   *
   * @return Response
   */
  public function sendEmail($email)
  {
      $user = $this->userManager->findUserByEmail($email);
      if (null === $user->getConfirmationToken()) {
          $user->setConfirmationToken($this->tokenGenerator->generateToken());
      }
      $this->mailer->sendResettingEmailMessage($user);
      $user->setPasswordRequestedAt(new \DateTime());
      $this->userManager->updateUser($user);
  }
}
