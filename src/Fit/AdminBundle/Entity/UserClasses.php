<?php

namespace Fit\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserClasses
 *
 * @ORM\Table(name="userClasses")
 * @ORM\Entity(repositoryClass="Fit\AdminBundle\Repository\UserClassesRepository")
 */
class UserClasses
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Fit\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Fit\AdminBundle\Entity\Classes")
     */
    private $classes;

    /**
     * @var int
     *
     * @ORM\Column(name="notif", type="integer")
     */
    private $notif;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return UserСlasses
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set classes
     *
     * @param integer $classes
     *
     * @return UserСlasses
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Get classes
     *
     * @return mixed
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Set notif
     *
     * @param integer $notif
     *
     * @return UserСlasses
     */
    public function setNotif($notif)
    {
        $this->notif = $notif;

        return $this;
    }

    /**
     * Get notif
     *
     * @return int
     */
    public function getNotif()
    {
        return $this->notif;
    }
}
