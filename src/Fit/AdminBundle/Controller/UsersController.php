<?php

namespace Fit\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Security\Core\Exception\NotFoundException;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Fit\UserBundle\Entity\User;

class UsersController extends Controller
{
    /*
    * User List
    */
    public function usersAction()
    {
      return $this->render('FitAdminBundle:Users:index.html.twig');
    }

    /*
    * New User Action
    */
    public function newAction(Request $req)
    {
      $user = new User();
      $form = $this->createFormBuilder($user)
          ->add('fullname', 'text')
          ->add('birthday', 'text')
          ->add('sex', ChoiceType::class, array(
                'choices'  => array(
                    'Male' => 1,
                    'Female' => 2,
                ),
                'choices_as_values' => true,
              ))
          ->add('email', EmailType::class)
          ->add('phone', 'text')
          ->add('imagePhoto', VichImageType::class, array(
              'label' => 'Photo'
          ))
          ->getForm();

      if($req->getMethod() == 'POST'){
        $form->handleRequest($req);
        if($form->isValid()){
            $em = $this->getDoctrine()->getEntityManager();
            $user->setEnabled(false);
            $user->setPassword('password');
            $user->setBirthday(new \DateTime($req->get('birthday')));
            $em->persist($user);
            $em->flush();
            $req->getSession()
                ->getFlashBag()
                ->add('success', 'Successfully created!')
            ;
            $reg = $this->container->get('fit_admin.reg');
            $reg->sendEmail($user->getEmail());
            return $this->redirect($this->generateUrl('admin_user_index'));
        }else{
          $req->getSession()
              ->getFlashBag()
              ->add('error', $form->getErrors())
          ;
        }
      }
      return $this->render('FitAdminBundle:Users:new.html.twig',[
        'form' => $form->createView(),
      ]);
    }

    /*
    * Edit Action
    */
    public function editAction(Request $req){
      $id = $req->get('id');
      $user = $this->getDoctrine()->getRepository('FitUserBundle:User')->findOneBy(['id' => $id]);
      if(!$user){ throw new NotFoundException("Not found"); }

      $user->setBirthday($user->getBirthday()->format('Y-m-d'));
      $form = $this->createFormBuilder($user)
          ->add('fullname', 'text')
          ->add('birthday', 'text')
          ->add('sex', ChoiceType::class, array(
                'choices'  => array(
                    'Male' => 1,
                    'Female' => 2,
                ),
                'choices_as_values' => true,
              ))
          ->add('enabled', ChoiceType::class, array(
                'choices'  => array(
                    'Active' => true,
                    'Unactive' => false,
                ),
                'choices_as_values' => true
              ))
          ->add('phone', 'text')
          ->add('imagePhoto', 'file', array(
              'label' => 'Photo',
              'required' => false,
          ))
          ->getForm();

      if($req->getMethod() == 'POST'){
        $form->handleRequest($req);
        if($form->isValid()){
            $em = $this->getDoctrine()->getEntityManager();
            $user->setBirthday(new \DateTime($req->get('birthday')));
            $em->persist($user);
            $em->flush();
            $req->getSession()
                ->getFlashBag()
                ->add('success', 'Successfully edited!')
            ;
            return $this->redirect($this->generateUrl('admin_user_index'));
        }else{
          $req->getSession()
              ->getFlashBag()
              ->add('error', $form->getErrors())
          ;
        }
      }
      return $this->render('FitAdminBundle:Users:edit.html.twig',[
        'form' => $form->createView(),
        'id' => $id,
      ]);
    }

    /*
    * Json response users
    */
    public function getUsersAction(Request $req){
      $s_echo = $req->get('sEcho');
      $i_display_start = $req->get('iDisplayStart');
      $i_display_length = $req->get('iDisplayLength');
      $i_sort_col_0 = $req->get('iSortCol_0');
      $i_sorting_cols = $req->get('iSortingCols');
      $s_search = $req->get('sSearch');

      $aColumns = array('u.id', 'u.email', 'u.fullname', 'u.sex', 'u.phone', 'u.birthday', 'u.enabled');
      $sOrder = '';

      if (isset($i_sort_col_0)) {
          $sOrder = 'ORDER BY  ';
          for ($i=0 ; $i<(int)$i_sorting_cols; $i++) {
              if ( $req->get( 'bSortable_'.(int)$req->get('iSortCol_'.$i) ) == 'true' ) {
                  $sOrder .= ''.$aColumns[ (int)$req->get('iSortCol_'.$i) ].' '.
                      ($req->get('sSortDir_'.$i) === 'ASC' ? 'ASC' : 'DESC') .', ';
              }
          }
          $sOrder = substr_replace($sOrder, '', -2);
          if ($sOrder == 'ORDER BY') {
              $sOrder = '';
          }
      }

      $sWhere = '';
      if (isset($s_search) && $s_search != '') {
          $sWhere = 'WHERE (';
          for ($i = 0; $i < count($aColumns); $i++) {
              if (null !== $req->get('bSearchable_'.$i) && $req->get('bSearchable_'.$i) == 'true') {
                  $sWhere .= '' . $aColumns[$i]." LIKE '".$s_search."' OR ";
              }
          }
          $sWhere = substr_replace($sWhere, '', -3);
          $sWhere .= ')';
      }

      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
                 "SELECT u
                 FROM FitUserBundle:User u
                 {$sWhere} {$sOrder}"
             );
      $query->setFirstResult((int)$i_display_start);
      $query->setMaxResults((int)$i_display_length);

      $users = $query->getResult();

      $qb = $em->createQuery(
                 "SELECT COUNT(u)
                 FROM FitUserBundle:User u
                 {$sWhere}"
             );
      $iCnt = $qb->getSingleScalarResult();

      $data = array('aaData' => array(),
        'iTotalRecords' => count($users),
        'sEcho' => $s_echo,
        'iTotalDisplayRecords' => $iCnt,
      );

      foreach ($users as $item) {
          switch ($item->isEnabled()) {
            case false:
              $status = '<span class="tag label label-danger">Unactivated</span>';
              break;
            case true:
              $status = '<span class="tag label label-success">Activate</span>';
              break;
            default:
              $status = '<span class="tag label label-danger">Error</span>';
              break;
          }

          switch ($item->getSex()) {
            case 1:
              $sex = '<span class="tag label label-success">Male</span>';
              break;
            case 2:
              $sex = '<span class="tag label label-primary">Female</span>';
              break;
            default:
              $sex = '<span class="tag label label-danger">Error</span>';
              break;
          }
          $url = $this->generateUrl('admin_user_edit', ['id' => $item->getId()]);
          $actions = '<a href="'.$url.'" class="btn btn-block btn-primary btn-xs">Edit</a>';

          $data['aaData'][] = array($item->getFullname(), $item->getEmail(), $item->getPhone(), $sex, $item->getBirthday()->format('d.m.Y'), $status, $actions);
      }

      $serializer = $this->container->get('jms_serializer');
      $res = $serializer->serialize($data, 'json');

      return new Response($res);
    }
}
