<?php

namespace Fit\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Security\Core\Exception\NotFoundException;
use Fit\AdminBundle\Entity\Classes;

class ClassesController extends Controller
{

    /*
    * Classes admin index page
    */
    public function classesAction()
    {
      return $this->render('FitAdminBundle:Classes:index.html.twig');
    }

    /*
    *  Json response Classes
    */
    public function getClassesAction(Request $req){
      $s_echo = $req->get('sEcho');
      $i_display_start = $req->get('iDisplayStart');
      $i_display_length = $req->get('iDisplayLength');
      $i_sort_col_0 = $req->get('iSortCol_0');
      $i_sorting_cols = $req->get('iSortingCols');
      $s_search = $req->get('sSearch');

      $aColumns = array('c.id', 'c.name', 'c.trainer', 'c.description');
      $sOrder = '';

      if (isset($i_sort_col_0)) {
          $sOrder = 'ORDER BY  ';
          for ($i=0 ; $i<(int)$i_sorting_cols; $i++) {
              if ( $req->get( 'bSortable_'.(int)$req->get('iSortCol_'.$i) ) == 'true' ) {
                  $sOrder .= ''.$aColumns[ (int)$req->get('iSortCol_'.$i) ].' '.
                      ($req->get('sSortDir_'.$i) === 'ASC' ? 'ASC' : 'DESC') .', ';
              }
          }
          $sOrder = substr_replace($sOrder, '', -2);
          if ($sOrder == 'ORDER BY') {
              $sOrder = '';
          }
      }

      $sWhere = '';
      if (isset($s_search) && $s_search != '') {
          $sWhere = 'WHERE (';
          for ($i = 0; $i < count($aColumns); $i++) {
              if (null !== $req->get('bSearchable_'.$i) && $req->get('bSearchable_'.$i) == 'true') {
                  $sWhere .= '' . $aColumns[$i]." LIKE '".$s_search."' OR ";
              }
          }
          $sWhere = substr_replace($sWhere, '', -3);
          $sWhere .= ')';
      }

      $em = $this->getDoctrine()->getManager();
      $query = $em->createQuery(
                 "SELECT c
                 FROM FitAdminBundle:Classes c
                 {$sWhere} {$sOrder}"
             );
      $query->setFirstResult((int)$i_display_start);
      $query->setMaxResults((int)$i_display_length);

      $classes = $query->getResult();

      $qb = $em->createQuery(
                 "SELECT COUNT(c)
                 FROM FitAdminBundle:Classes c
                 {$sWhere}"
             );
      $iCnt = $qb->getSingleScalarResult();

      $data = array('aaData' => array(),
        'iTotalRecords' => count($classes),
        'sEcho' => $s_echo,
        'iTotalDisplayRecords' => $iCnt,
      );

      foreach ($classes as $item) {
          $scout = $em->createQuery(
                   "SELECT COUNT(c)
                   FROM FitAdminBundle:UserClasses c
                   WHERE c.classes = ".$item->getId()
               );
          $surl = $this->generateUrl('admin_classes_show', ['id' => $item->getId()]);
          $eurl = $this->generateUrl('admin_classes_edit', ['id' => $item->getId()]);
          $actions = '<a href="'.$surl.'" class="btn btn-success btn-xs">Show</a> <a href="'.$eurl.'" class="btn btn-primary btn-xs">Edit</a> <button id="'.$item->getId().'" class="btn btn-danger btn-xs delete">Delete</button>';
          $clients = '<span class="tag label label-primary">'.$scout->getSingleScalarResult().'</span>';
          $data['aaData'][] = array($item->getName(), $item->getTrainer(), $item->getDescription(), $clients, $actions);
      }

      $serializer = $this->container->get('jms_serializer');
      $res = $serializer->serialize($data, 'json');

      return new Response($res);
    }

    /*
    * New Classes Action
    */
    public function newAction(Request $req){
      $classes = new Classes();
      $form = $this->createFormBuilder($classes)
          ->add('name', 'text')
          ->add('trainer', 'text')
          ->add('description', TextareaType::class)
          ->getForm();

      if($req->getMethod() == 'POST'){
        $form->handleRequest($req);
        if($form->isValid()){
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($classes);
            $em->flush();
            $req->getSession()
                ->getFlashBag()
                ->add('success', 'Successfully created!')
            ;
            return $this->redirect($this->generateUrl('admin_classes_index'));
        }else{
          $req->getSession()
              ->getFlashBag()
              ->add('error', $form->getErrors())
          ;
        }
      }

      return $this->render('FitAdminBundle:Classes:new.html.twig', [
        'form' => $form->createView(),
      ]);
    }

    /*
    * Edit Classes Action
    */
    public function editAction(Request $req){
      $id = $req->get('id');
      $class = $this->getDoctrine()->getRepository('FitAdminBundle:Classes')->findOneBy(['id' => $id]);
      if(!$class){ throw new NotFoundException("Not found"); }
      $form = $this->createFormBuilder($class)
          ->add('name', 'text')
          ->add('trainer', 'text')
          ->add('description', TextareaType::class)
          ->getForm();

      if($req->getMethod() == 'POST'){
        $form->handleRequest($req);
        if($form->isValid()){
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($classes);
            $em->flush();
            $req->getSession()
                ->getFlashBag()
                ->add('success', 'Successfully edited!')
            ;
            return $this->redirect($this->generateUrl('admin_classes_index'));
        }else{
          $req->getSession()
              ->getFlashBag()
              ->add('error', $form->getErrors())
          ;
        }
      }

      return $this->render('FitAdminBundle:Classes:edit.html.twig', [
        'form' => $form->createView(),
        'id' => $id
      ]);
    }

    /*
    * Remove Classes Action
    */
    public function removeAction(Request $req){
      $id = $req->get('id');

      $class = $this->getDoctrine()->getRepository('FitAdminBundle:Classes')->findOneBy(['id' => $id]);
      if($class){
        $em = $this->getDoctrine()->getManager();
        $subs = $this->getDoctrine()->getRepository('FitAdminBundle:UserClasses')->findBy(['classes' => $class->getId()]);
        if($subs){
          foreach ($subs as $item) {
            $em->remove($item);
            $em->flush();
          }
        }
        $em->remove($class);
        $em->flush();
        $data = ['status' => 'success'];
      }else{
        $data = ['status' => 'error'];
      }

      $serializer = $this->container->get('jms_serializer');
      $res = $serializer->serialize($data, 'json');

      return new Response($res);
    }

    /*
    * Show Action
    */
    public function showAction(Request $req)
    {
      $id = $req->get('id');
      $class = $this->getDoctrine()->getRepository('FitAdminBundle:Classes')->findOneBy(['id' => $id]);
      if(!$class){ throw new NotFoundException("Not found"); }

      return $this->render('FitAdminBundle:Classes:show.html.twig', [
        'title' => $class->getName(),
        'trainer' => $class->getTrainer(),
        'description' => $class->getDescription(),
        'id' => $id
      ]);
    }
}
