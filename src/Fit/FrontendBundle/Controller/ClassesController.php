<?php
namespace Fit\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Fit\AdminBundle\Entity\UserClasses;

/**
 * Classes controller
 */
class ClassesController extends Controller
{
  /*
  * Get Classes function
  */
  public function getClassesAction(Request $req){
    $s_echo = $req->get('sEcho');
    $i_display_start = $req->get('iDisplayStart');
    $i_display_length = $req->get('iDisplayLength');
    $i_sort_col_0 = $req->get('iSortCol_0');
    $i_sorting_cols = $req->get('iSortingCols');
    $s_search = $req->get('sSearch');

    $securityContext = $this->container->get('security.authorization_checker');
    if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        $user = $this->getUser();
    } else {
      throw new AccessDeniedException("Access Denied");
    }

    $aColumns = array('c.id', 'c.name', 'c.trainer', 'c.description');
    $sOrder = '';

    if (isset($i_sort_col_0)) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$i_sorting_cols; $i++) {
            if ( $req->get( 'bSortable_'.(int)$req->get('iSortCol_'.$i) ) == 'true' ) {
                $sOrder .= ''.$aColumns[ (int)$req->get('iSortCol_'.$i) ].' '.
                    ($req->get('sSortDir_'.$i) === 'ASC' ? 'ASC' : 'DESC') .', ';
            }
        }
        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    $sWhere = '';
    if (isset($s_search) && $s_search != '') {
        $sWhere = 'WHERE (';
        for ($i = 0; $i < count($aColumns); $i++) {
            if (null !== $req->get('bSearchable_'.$i) && $req->get('bSearchable_'.$i) == 'true') {
                $sWhere .= '' . $aColumns[$i]." LIKE '".$s_search."' OR ";
            }
        }
        $sWhere = substr_replace($sWhere, '', -3);
        $sWhere .= ')';
    }

    $em = $this->getDoctrine()->getManager();
    $query = $em->createQuery(
               "SELECT c
               FROM FitAdminBundle:Classes c
               {$sWhere} {$sOrder}"
           );
    $query->setFirstResult((int)$i_display_start);
    $query->setMaxResults((int)$i_display_length);

    $classes = $query->getResult();

    $qb = $em->createQuery(
               "SELECT COUNT(c)
               FROM FitAdminBundle:Classes c
               {$sWhere}"
           );
    $iCnt = $qb->getSingleScalarResult();

    $data = array('aaData' => array(),
      'iTotalRecords' => count($classes),
      'sEcho' => $s_echo,
      'iTotalDisplayRecords' => $iCnt,
    );

    foreach ($classes as $item) {
        $sub = $this->getDoctrine()->getRepository('FitAdminBundle:UserClasses')->findOneBy(['user' => $user, 'classes' => $item->getId()]);
        if($sub){
          $actions = '<button type="button" id="'.$item->getId().'" class="btn btn-danger btn-xs unsubscribe">Unsubscribe</button> <button type="button" id="'.$item->getId().'" class="btn btn-success btn-xs notif">Notification</button>';
        }else{
          $actions = '<button type="button" id="'.$item->getId().'" class="btn btn-block btn-success btn-xs subscribe">Subscribe</button>';
        }
        $data['aaData'][] = array($item->getName(), $item->getTrainer(), $item->getDescription(), $actions);
    }

    $serializer = $this->container->get('jms_serializer');
    $res = $serializer->serialize($data, 'json');

    return new Response($res);
  }

  /*
  * Subscribe to class function
  */
  public function subscribeClassAction(Request $req){
    $id = $req->get('id');

    $securityContext = $this->container->get('security.authorization_checker');
    if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        $user = $this->getUser();
    } else {
      throw new AccessDeniedException("Access Denied");
    }

    $serializer = $this->container->get('jms_serializer');
    $sub = $this->getDoctrine()->getRepository('FitAdminBundle:UserClasses')->findOneBy(['user' => $user, 'classes' => $id]);
    if($sub){
      $data = ['status' => 'error'];
      $res = $serializer->serialize($data, 'json');
      return new Response($res);
    }

    $class = $this->getDoctrine()->getRepository('FitAdminBundle:Classes')->findOneBy(['id' => $id]);

    if($class){
      $em = $this->getDoctrine()->getManager();
      $sub = new UserClasses();
      $sub->setUser($user);
      $sub->setClasses($class);
      $sub->setNotif(0);
      $em->persist($sub);
      $em->flush();
      $data = ['status' => 'success'];
    }else{
      $data = ['status' => 'error'];
    }

    $res = $serializer->serialize($data, 'json');

    return new Response($res);
  }

  /*
  * Unsubscribe Class function
  */
  public function unsubscribeClassAction(Request $req){
    $id = $req->get('id');
    $securityContext = $this->container->get('security.authorization_checker');
    if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        $user = $this->getUser();
    } else {
      throw new AccessDeniedException("Access Denied");
    }

    $sub = $this->getDoctrine()->getRepository('FitAdminBundle:UserClasses')->findOneBy(['user' => $user, 'classes' => $id]);

    if($sub){
      $em = $this->getDoctrine()->getEntityManager();
      $em->remove($sub);
      $em->flush();
      $data = ['status' => 'success'];
    }else{
      $data = ['status' => 'error'];
    }

    $serializer = $this->container->get('jms_serializer');
    $res = $serializer->serialize($data, 'json');

    return new Response($res);
  }

  public function getNotifAction(Request $req){
    $id = $req->get('id');
    $securityContext = $this->container->get('security.authorization_checker');
    if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        $user = $this->getUser();
    } else {
      throw new AccessDeniedException("Access Denied");
    }

    $sub = $this->getDoctrine()->getRepository('FitAdminBundle:UserClasses')->findOneBy(['user' => $user, 'classes' => $id]);

    if($sub){
      $data = ['status' => 'success', 'notif' => $sub->getNotif()];
    }else{
      $data = ['status' => 'error'];
    }
    $serializer = $this->container->get('jms_serializer');
    $res = $serializer->serialize($data, 'json');

    return new Response($res);
  }

  public function setNotifAction(Request $req){
    $id = $req->get('id');
    $notif = $req->get('notif');

    $securityContext = $this->container->get('security.authorization_checker');
    if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        $user = $this->getUser();
    } else {
      throw new AccessDeniedException("Access Denied");
    }

    $sub = $this->getDoctrine()->getRepository('FitAdminBundle:UserClasses')->findOneBy(['user' => $user, 'classes' => $id]);

    if($sub){
      $em = $this->getDoctrine()->getManager();
      $sub->setNotif($notif);
      $em->persist($sub);
      $em->flush();
      $data = ['status' => 'success'];
    }else{
      $data = ['status' => 'error'];
    }
    $serializer = $this->container->get('jms_serializer');
    $res = $serializer->serialize($data, 'json');

    return new Response($res);
  }
}
