<h1>Fitness Club</h1>
=======

<a href="https://yadi.sk/i/H1SRM8EP3XuiXG" target="_blank">Задание тут.</a>

<h5>Requirements</h5>
<blockquote>
<p>
  php >= 5.6<br/>
  RabbitMQ<br/>
</p>
</blockquote>

<h5>Installation:</h5>
<blockquote>
  git clone https://gitlab.com/StivR/fitnessclub.git
  <br /><br />
  cd fitnessclub
  <br /><br />
  composer install
  <br /><br />
  ./app/console doctrine:schema:update --force
  <br /><br />
  ./app/console doctrine:migrations:migrate
</blockquote>

<h5>Usage:</h5>

<blockquote>
  ./app/console server:run
  <br /><br />
  ./app/console rabbitmq:consumer send_email
  <br /><br />
  ./app/console rabbitmq:consumer send_sms
  <br />
</blockquote>

<blockquote>
  <p>
    Для проверки почты можно использовать:
      mailer_host: mx1.hostinger.ru
      mailer_user: support@fitdemo.ru
      mailer_password: 1q2w3e4r
    Для SMS рассылки есть 4 попытки.
  </p>
</blockquote>
